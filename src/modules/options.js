export const titleByRoutePath = {
  '': 'Appointments',
  editor: 'Make appointment'
}

export const dateFormat = 'MMM DD, YYYY'

export const checkBank = {
  name: v => !!v.length,
  date: v => !!v
}

export const emptyAppointment = {
  name: '',
  date: null,
  complete: false,
  note: ''
}
