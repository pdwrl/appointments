import { emptyAppointment } from '@/modules/options'

export default {
  appointments: [],
  newAppointment: Object.assign({}, emptyAppointment)
}
