import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'
import mutations from './mutations'
import getters from './getters'
import state from './state'

Vue.use(Vuex)
const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
  key: 'AppointmentsStorage'
})

export default function () {
  const Store = new Vuex.Store({
    plugins: [vuexLocal.plugin],
    state,
    mutations,
    getters,

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  return Store
}
