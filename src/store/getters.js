import { checkBank } from '@/modules/options'

export default {
  readyToSaveNewAppointment (state) {
    const newAppointment = state.newAppointment
    const checkBankKeys = Object.keys(checkBank)
    const okKeys = checkBankKeys
      .filter(key => {
        const checkBankFunc = checkBank[key]
        return checkBankFunc(newAppointment[key])
      })
    return okKeys.length === checkBankKeys.length
  }
}
