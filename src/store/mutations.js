import { emptyAppointment } from '@/modules/options'

export default {
  setComplete (state, { index, value }) {
    state.appointments[index].complete = value
  },

  confirmNewAppointment (state) {
    const allIds = state.appointments
      .map(item => item.id)
    const maxId = Math.max(...allIds)
    const newId = maxId + 1
    const newAppointment = Object.assign({}, state.newAppointment, {
      id: newId
    })

    state.appointments
      .unshift(newAppointment)

    Object.assign(state.newAppointment, emptyAppointment)
  },

  changeNewAppointment (state, { name, value }) {
    state.newAppointment[name] = value
  },

  removeAppointment (state, { index }) {
    state.appointments.splice(index, 1)
  }
}
