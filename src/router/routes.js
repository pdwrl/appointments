import AppointmentsList from '@/pages/AppointmentsList'
import AppointmentsEditor from '@/pages/AppointmentsEditor/'
import TabInfo from '@/pages/AppointmentsEditor/TabInfo'
import TabConfirmation from '@/pages/AppointmentsEditor/TabConfirmation'

const routes = [
  {
    path: '/',
    name: 'AppointmentsList',
    component: AppointmentsList
  },
  {
    path: '/editor',
    name: 'AppointmentsEditor',
    component: AppointmentsEditor,
    redirect: { name: 'Info' },
    children: [
      {
        path: '/editor/info',
        name: 'Info',
        components: { tabView: TabInfo }
      },
      {
        path: '/editor/confirmation',
        name: 'Confirmation',
        components: { tabView: TabConfirmation }
      }
    ]
  },
  {
    path: '*', // 404
    redirect: { name: 'AppointmentsList' }
  }
]

export default routes
